const banners = document.querySelectorAll("img");
const stopSlider = document.querySelector(".stop-button");
const continueSlider = document.querySelector(".continue-button");
let showbanners = 0;
let sliderWorks = false;

function slider() {
    banners[showbanners].className = "image-to-show";
    if (showbanners = (showbanners + 1) % banners.length);
    banners[showbanners].classList.add("show");
}

let sliderShow = setInterval(slider, 3000, (sliderWorks = true));
stopSlider.addEventListener('click', pause);

function pause() { 
    clearInterval(sliderShow);
    sliderWorks = false;
}

continueSlider.addEventListener('click', next); 

function next() {
    if (!sliderWorks) {
        sliderShow = setInterval(slider, 3000, (sliderWorks = true));
    }
} 